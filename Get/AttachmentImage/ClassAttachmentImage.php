<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\AttachmentImage;

use WPezSuite\WPezAPI\Get\Attachment\ClassAttachment;
use WPezSuite\WPezAPI\Get\AttachmentImageMeta\ClassAttachmentImageMeta;
use WPezSuite\WPezAPI\Get\Images\ClassImages;

class ClassAttachmentImage extends ClassAttachment {

    protected $_obj_image_meta;

    //
    protected $_str_size_original_name;


    public function __construct() {

        $this->setPropertyDefaultsSelf();

        parent::__construct();

    }

    protected function setPropertyDefaultsSelf() {

        $this->_obj_image_meta             = false;
        $this->_bool_icon                  = false;


        //

        $this->_str_size_original_name = 'original'; // TODO add set'er
    }

    protected function setPostCheck( $obj_post ) {

        return wp_attachment_is( 'image', $obj_post );
    }


    // TODO - revisit
    public function setImageByPostID( $mix = false ) {

        if ( ! $mix instanceof \WP_Post ) {
            $mix_get_post = get_post( $mix );
        } else {
            $mix_get_post = $mix;
        }

        if ( $mix_get_post instanceof \WP_Post ) {
            $int_id = get_post_thumbnail_id( $mix_get_post->ID );
            if ( ! empty( $int_id ) ) {
                return $this->setPostByID( $int_id );
            }
        }

        return false;
    }

    // protected function magicGetProxyImage( $str_prop = '' ) {
    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'alt':
            case 'alt_text':
            case 'alternative_text':
                return $this->getAlt();

            case 'images':
                return $this->getImages();

            case 'image_meta':
                return $this->getImageMeta();

            default:

                return parent::__get($str_prop);
        }

    }

    public function getAlt( $mix_fallback = '' ) {

        $str_alt = $this->getMeta()->_wp_attachment_image_alt;
        if ( ! empty( $str_alt ) ) {
            return $str_alt;
        }

        return $mix_fallback;
    }

    public function getImages() {

        $new      = new ClassImages();
        $bool_ret = $new->setAttachmentImageID( $this->_int_id );

        if ( $bool_ret === true ) {
            return $new;
        }

        return false;
    }

    public function getImageMeta() {

        if ( $this->_obj_image_meta instanceof ClassAttachmentImageMeta ) {
            return $this->_obj_image_meta;
        }

        $arr_att_meta = $this->getMeta()->_wp_attachment_metadata;
        if ( isset( $arr_att_meta['image_meta'] ) && is_array( $arr_att_meta['image_meta'] ) ) {

            $new = new ClassAttachmentImageMeta();
            $new->loaderAll( $arr_att_meta['image_meta'] );
            $this->_obj_image_meta = $new;

        }

        return $this->_obj_image_meta;

    }

}
