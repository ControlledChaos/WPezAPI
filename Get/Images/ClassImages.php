<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezAPI\Get\Images;

use WPezSuite\WPezAPI\Get\Image\ClassImage;

class ClassImages {

    protected $_int_attachment_id;
    protected $_arr_wp_attachment_metadata;
    protected $_str_name_original;
    protected $_str_mime_type;
    protected $_str_size;
    protected $_arr_sizes;
    protected $_arr_get_image; // ???

    // --------

    protected $_str_size_original_name;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_arr_wp_attachment_metadata = false;
        $this->_str_name_original          = 'original';
        $this->_str_mime_type              = false;
        $this->_str_size                   = false;
        $this->_arr_sizes                  = false;

        $this->_str_size_original_name = 'original'; // TODO add set'er

    }

    public function setAttachmentImageID( $mix = false ) {

        if ( ( is_string( $mix ) || is_integer( $mix ) ) && wp_attachment_is( 'image', (integer)$mix ) ) {
            $this->_int_attachment_id = (integer)$mix;
            $mix_ret                  = wp_get_attachment_metadata( $mix );
            if ( is_array( $mix_ret ) ) {
                $this->_arr_wp_attachment_metadata = $mix_ret;
                if ( isset( $this->_arr_wp_attachment_metadata['sizes'] ) && is_array( $this->_arr_wp_attachment_metadata['sizes'] ) ) {

                    reset( $this->_arr_wp_attachment_metadata['sizes'] );
                    $str_key = key( $this->_arr_wp_attachment_metadata['sizes'] );
                    if ( isset( $this->_arr_wp_attachment_metadata['sizes'][ $str_key ]['mime-type'] ) ) {
                        // we want to store the
                        $this->_str_mime_type = $this->_arr_wp_attachment_metadata['sizes'][ $str_key ]['mime-type'];
                    }
                    reset( $this->_arr_wp_attachment_metadata['sizes'] );
                }

                return true;
            }
        }

        return false;

    }

    /**
     * If you already have a registered image size name of 'original' change it to something else (e.g., full)
     *
     * @param string $str_name
     *
     * @return bool
     */
    public function setNameOriginal( $str_name = 'original' ) {

        $str_name = trim( $str_name );
        if ( is_string( $str_name ) && ! empty( $str_name ) && $this->hasSize( $str_name ) === false ) {

            $this->_str_name_original = $str_name;

            return true;
        }

        return false;

    }

    public function setImageSize( $str_size = false ) {

        $this->_str_size = (string)$str_size;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'all':
                return $this->getAll();

            case 'sizes':
                return $this->getSizes();

            // as based on size set by setImageSize()
            case 'image':
                return $this->getImage();

            default:
                /**
                 * you can access a given image size directly BUT __only if__:
                 *
                 * 1) that size name is not a "property" name (else it'll never make it this far.
                 * 2) the size names use _ and not - (or any char that prevents it from being treated like a "property"
                 *
                 * else use the method: getSize('some-size')
                 */

                $arr_sizes = $this->getSizes();
                if ( isset( $arr_sizes[ $str_prop ] ) ) {
                    return $this->getImage( $str_prop );
                }

                return false;

        }
    }

    public function hasSize( $str_size = false ) {

        $str_size = trim( $str_size );

        return isset( $this->getSizes()[ $str_size ] );
    }


    public function hasSizes( $arr_sizes = [] ) {

        if ( is_array( $arr_sizes ) && array_intersect( $arr_sizes, $this->getSizes( 'keys' ) ) == $arr_sizes ) {
            return true;
        }

        return false;
    }


    public function getSizes( $str = 'all' ) {

        if ( is_array( $this->_arr_sizes ) ) {
            if ( $str === 'keys' ) {
                return array_keys( $this->_arr_sizes );
            }

            return $this->_arr_sizes;
        }

        $this->_arr_sizes = [];
        if ( isset( $this->_arr_wp_attachment_metadata['sizes'] ) && is_array( $this->_arr_wp_attachment_metadata['sizes'] ) ) {
            $this->_arr_sizes = $this->_arr_wp_attachment_metadata['sizes'];
        }
        // add the original / full size image to the array of sizes
        $mix_orig = $this->getOriginal();
        if ( is_array( $mix_orig ) ) {
            $this->_arr_sizes[ $this->_str_name_original ] = $mix_orig;

        }

        if ( $str === 'keys' ) {
            return array_keys( $this->_arr_sizes );
        }

        return $this->_arr_sizes;
    }


    protected function getOriginal() {

        if ( isset( $this->_arr_wp_attachment_metadata['width'] ) && ! empty( $this->_arr_wp_attachment_metadata['width'] )
             && isset( $this->_arr_wp_attachment_metadata['height'] )
             && ! empty( $this->_arr_wp_attachment_metadata['height'] )
             && isset( $this->_arr_wp_attachment_metadata['file'] )
             && ! empty( $this->_arr_wp_attachment_metadata['file'] ) ) {

            $arr_size              = [];
            $arr_size['file']      = basename( $this->_arr_wp_attachment_metadata['file'] );
            $arr_size['width']     = $this->_arr_wp_attachment_metadata['width'];
            $arr_size['height']    = $this->_arr_wp_attachment_metadata['height'];
            $arr_size['mime_type'] = $this->_str_mime_type;

            return $arr_size;
        }

        return false;


    }


    public function getAll() {

        $mix = $this->getSizes( 'keys' );
        if ( is_array( $mix ) ) {

            foreach ( $mix as $str_size ) {

                $this->_arr_get_image[ $str_size ] = $this->getImage( $str_size );
            }

            return $this->_arr_get_image;

        }

        return false;
    }


    public function getImage( $str_size = '', $bool_icon = false ) {

        // https://developer.wordpress.org/reference/functions/wp_get_attachment_image/

        // TODO - if blank, return original
        $str_size = trim( $str_size );
        if ( isset( $this->_arr_get_image[ $str_size ] ) && isset( $this->_arr_get_image[ $str_size ] ) instanceof ClassImage ) {
            return $this->_arr_get_image[ $str_size ];
        }

        if ( $this->hasSize( $str_size ) === false ) {
            return false;
        }

        $new      = new ClassImage();
        $bool_ret = $new->setAttachmentImageID( $this->_int_attachment_id );
        if ( $bool_ret === true ) {
            $new->setSize( $str_size );
            $this->_arr_get_image[ $str_size ] = $new;

            return $new;
        }

        return false;

    }


}