<?php
/**
 * WordPress' get_avatar() function wrapped up as an ez to use class. Establish
 * so it can be reference from a post (object) or a comment (object), or stand
 * alone. https://codex.wordpress.org/Function_Reference/get_avatar
 */

/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\GravatarProfile;


class ClassGravatarProfile {

    protected $_str_gravatar_com;
    protected $_arr_profile_formats;
    protected $_int_qr_size_default;
    protected $_str_user_email;
    protected $_str_user_email_md5;
    protected $_arr_json;
    protected $_arr_php;
    protected $_str_id;
    protected $_str_hash;
    protected $_str_profile_url;
    protected $_str_username;
    protected $_str_thumbnail_url;
    protected $_str_display_name;
    protected $_arr_photos;
    protected $_arr_urls;
    protected $_str_qr_url;
    protected $_str_vcf_url;
    protected $_str_xml;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    /**
     *
     */
    protected function setPropertyDefaults() {

        $this->_str_gravatar_com    = 'https://www.gravatar.com/';
        $this->_arr_profile_formats = [ 'json', 'php', 'xml' ];
        $this->_int_qr_size_default = 200;
        $this->_str_user_email      = false;
        $this->_str_user_email_md5  = false;
        $this->_arr_json            = false;
        $this->_arr_php             = false;
        $this->_str_id              = false;
        $this->_str_hash            = false;
        $this->_str_profile_url     = false;
        $this->_str_username        = false;
        $this->_str_thumbnail_url   = false;
        $this->_str_display_name    = false;
        $this->_arr_photos          = false;
        $this->_arr_urls            = false;
        $this->_str_qr_url          = false;
        $this->_str_vcf_url         = false;
        $this->_str_xml             = false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_email
     *
     * @return bool
     */
    public function setUserEmail( $str_email = false ) {

        $str_email = trim( $str_email );
        if ( get_avatar_url( $str_email ) !== false ) {
            $this->_str_user_email     = $str_email;
            $this->_str_user_email_md5 = md5( $this->_str_user_email );

            return true;
        }

        return false;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'qr':
            case 'qr_code':
            case 'qr_code_url':
            case 'qr_code_src':
                return $this->getQR();

            case 'vcf':
            case 'vcard':
            case 'vcf_url':
            case 'vcard_url':
                return $this->getVCF();

            case 'id':
                return $this->getID();

            case 'hash':
                return $this->getHash();

            case 'profile_url':
                return $this->getProfileURL();

            case 'username':
                return $this->getUsername();

            case 'thumbnail_url':
                return $this->getThumbnailURL();

            case 'display_name':
                return $this->getDisplayName();

            case 'photos':
                return $this->getPhotos();

            case 'urls':
                return $this->getURLs();

            default:
                // TODO
                return false;

        }

    }

    public function getID() {

        if ( $this->_str_id === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['id'] ) ) {

                $this->_str_id = $mix['entry'][0]['id'];
            } else {
                $this->_str_id = '';
            }

        }

        return $this->_str_id;
    }

    public function getHash() {

        if ( $this->_str_hash === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['hash'] ) ) {

                $this->_str_hash = $mix['entry'][0]['hash'];
            } else {
                $this->_str_hash = '';
            }

        }

        return $this->_str_hash;
    }

    public function getProfileURL() {

        if ( $this->_str_profile_url === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['profileUrl'] ) ) {

                $this->_str_profile_url = $mix['entry'][0]['profileUrl'];
            } else {
                $this->_str_profile_url = '';
            }

        }

        return $this->_str_profile_url;
    }


    public function getUsername() {

        if ( $this->_str_username === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['preferredUsername'] ) ) {

                $this->_str_username = $mix['entry'][0]['preferredUsername'];
            } else {
                $this->_str_username = '';
            }

        }

        return $this->_str_username;
    }

    public function getThumbnailURL() {

        if ( $this->_str_thumbnail_url === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['thumbnailUrl'] ) ) {

                $this->_str_thumbnail_url = $mix['entry'][0]['thumbnailUrl'];
            } else {
                $this->_str_thumbnail_url = '';
            }

        }

        return $this->_str_thumbnail_url;
    }

    public function getDisplayName() {

        if ( $this->_str_display_name === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['displayName'] ) ) {

                $this->_str_display_name = $mix['entry'][0]['displayName'];
            } else {
                $this->_str_display_name = '';
            }

        }

        return $this->_str_display_name;
    }


    public function getPhotos() {

        if ( $this->_arr_photos === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['photos'] ) ) {

                $this->_arr_photos = $mix['entry'][0]['photos'];
            } else {
                $this->_arr_photos = [];
            }

        }

        return $this->_arr_photos;
    }


    public function getURLs() {

        if ( $this->_arr_urls === false ) {
            $mix = $this->getProfile();

            if ( is_array( $mix ) && isset( $mix['entry'][0]['urls'] ) ) {

                $this->_arr_urls = $mix['entry'][0]['urls'];
            } else {
                $this->_arr_urls = [];
            }

        }

        return $this->_arr_urls;
    }


    /**
     * Note: You __must__ use setUserEmail() as email is Gravatar's "key" to
     * requesting a profile.
     *
     * @return bool
     */
    public function getProfile( $str_ret = 'php' ) {

        $str_ret = trim( strtolower( $str_ret ) );
        if ( in_array( $str_ret, $this->_arr_profile_formats ) && is_string( $this->_str_user_email ) ) {

            switch ( $str_ret ) {
                case 'php':
                    if ( is_array( $this->_arr_php ) ) {
                        return $this->_arr_php;
                    }
                    break;

                case 'json':
                    if ( is_array( $this->_arr_json ) ) {
                        return $this->_arr_json;
                    }
                    break;
                case 'xml':
                    if ( is_string( $this->_str_xml ) ) {
                        return $this->_str_xml;
                    }
                    break;

                default:
                    // if you make a bad request, you get a false
                    // TODO - default returns php?
                    return false;

            }

            $str = file_get_contents( $this->_str_gravatar_com . $this->_str_user_email_md5 . '.' . $str_ret );

            // ['json','php', 'xml' ];
            if ( $str_return = 'json' ) {

                // http://en.gravatar.com/site/implement/profiles/json/
                // https://secure.php.net/manual/en/function.json-decode.php
                $mix_profile = json_decode( $str, true );
                if ( is_array( $mix_profile ) && isset( $mix_profile['entry'] ) ) {

                    $this->_arr_json = $mix_profile;

                    return $this->_arr_json;

                }
                // return false;


            } elseif ( $str_ret = 'php' ) {

                // http://en.gravatar.com/site/implement/profiles/php/
                $mix_profile = unserialize( $str );
                if ( is_array( $mix_profile ) && isset( $mix_profile['entry'] ) ) {

                    $this->_arr_php = $mix_profile;

                    return $this->_arr_php;

                }
                // return false;
            } elseif ( $str = 'xml' ) {

                // http://en.gravatar.com/site/implement/profiles/xml/
                $xml   = simplexml_load_string( $str );
                $entry = $xml->xpath( '//entry' );
                if ( is_array( $entry ) ) {

                    $this->_str_xml = $xml;

                    return $this->_str_xml;
                }
                // return false;
            }
        }

        return false;
    }


    /**
     * "The only data currently encoded in the QR Code is a link back to the
     * profile page." http://en.gravatar.com/site/implement/profiles/qr/
     *
     * @param string $str_width
     */
    public function getQR( $mix_width = false ) {

        if ( is_string( $this->_str_user_email ) && $this->_str_qr_url === false ) {

            $str_width = $this->_int_qr_size_default;
            if ( is_numeric( $mix_width ) ) {
                $str_width = (integer)$mix_width;
                if ( $str_width > 547 ) {
                    // seems to be the max gravatar.com will return. TODO = test more
                    $str_width = 547;
                }
            }
            $str_width         = '?s=' . $str_width;
            $this->_str_qr_url = $this->_str_gravatar_com . $this->_str_user_email_md5 . '.qr' . $str_width;
        }

        return $this->_str_qr_url;
    }

    /**
     * @return string
     */
    public function getVCF() {

        if ( is_string( $this->_str_user_email ) && $this->_str_vcf_url === false ) {

            $this->_str_vcf_url = $this->_str_gravatar_com . $this->_str_user_email_md5 . '.vcf';

        }

        return $this->_str_vcf_url;


    }


}
