<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\PostBase;

use WPezSuite\WPezAPI\Get\Author\ClassAuthor;
use WPezSuite\WPezAPI\Get\Meta\ClassMeta;
use WPezSuite\WPezAPI\Get\Taxonomies\ClassTaxonomies;

abstract class AbstractClassPostBase {

    use \WPezSuite\WPezAPI\Get\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_bool_active;
    protected $_int_id;
    protected $_obj_post;

    protected $_str_content_filtered;
    protected $_str_permalink;
    protected $_str_shortlink;
    protected $_arr_post_class;
    protected $_int_author_id;
    protected $_obj_author;

    protected $_obj_meta;
    protected $_obj_taxs;

    public function __construct( $mix = false ) {

        $this->setPropertyDefaults();

        if ( $mix !== false ) {
            $this->setPostByID( $mix );
        }

    }


    protected function setPropertyDefaults() {

        $this->_bool_active          = false;
        $this->_int_id               = false;
        $this->_obj_post             = false;
        $this->_str_content_filtered = false;
        $this->_str_permalink        = false;
        $this->_str_shortlink        = false;
        $this->_arr_post_class       = false;
        $this->_int_author_id        = false;
        $this->_obj_author           = false;
        $this->_obj_meta             = false;
        $this->_obj_taxs             = false;
    }


    /**
     * property if the set / load isn't right then use $bool_active to return
     * false for property requests
     *
     * @param bool $mix
     *
     * @return bool
     */
    public function setPostByID( $mix = false, $str_post_type = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_Post ) {

                $mix_get_post = $mix;

            } else {
                $mix_get_post = get_post( $mix );
            }

            if ( $mix_get_post instanceof \WP_Post && $this->setPostCheck( $mix_get_post ) ) {
                if ( is_string( $str_post_type ) && $mix_get_post->post_type !== $str_post_type ) {
                    $this->_bool_active = false;

                    return false;
                }
                $this->_obj_post      = $mix_get_post;
                $this->_int_id        = $mix_get_post->ID;
                $this->_int_author_id = $mix_get_post->post_author;
                $this->_bool_active   = true;

                return true;
            }
        }

        $this->_bool_active = false;

        return false;
    }

    abstract protected function setPostCheck( $obj_post );

    /**
     * these can be used across all descendents of this class
     *
     * @param string $str_prop
     *
     * @return bool|string|ClassMeta|ClassTaxonomies|ClassAuthor
     */
    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'active':
                return $this->getActive();

            case 'id':
                return $this->getID();

            case 'post':
                return $this->getPost();

            // Note: author (see below) will return a author (user) object
            case 'author_id':
                return $this->getAuthorID();

            case 'date':
                return $this->getDate();

            case 'date_gmt':
                return $this->getDateGMT();

            case 'title':
                return $this->getTitle();

            case 'status':
                return $this->getStatus();

            case 'status_comment':
            case 'comment_status':
                return $this->getStatusComment();

            case 'status_ping':
            case 'ping_status':
                return $this->getStatusPing();

            case 'password':
            case 'pwd':
                return $this->getPassword();

            case 'slug':
            case 'name':
                return $this->getSlug();

            case 'to_ping':
                return $this->getToPing();

            case 'pinged':
                return $this->getPinged();

            case 'modified':
                return $this->getModified();

            case 'modified_gmt':
                return $this->getModifiedGMT();

            case 'parent':
                return $this->getParent();

            case 'guid':
                return $this->getGUID();

            case 'order':
            case 'menu_order':
                return $this->getOrder();

            case 'type':
            case 'post_type':
                return $this->getType();

            case 'mime_type':
                return $this->getMimeType();

            case 'comment_count':
                return $this->getCommentCount();

            case 'filter':
                return $this->getFilter();

            case 'url':
            case 'link':
            case 'permalink':
                return $this->getURL();

            case 'url_short':
            case 'shortlink':
                return $this->getURLShort();

            case 'class':
                return $this->getClass();


            /*
             * ------ objects ------
             */

            case 'author':
                return $this->getAuthor();

            case 'comments':
                return 'TODO';

            case 'meta':
                return $this->getMeta();

            case 'taxonomies':
                return $this->getTaxonomies();

            default:

                return '';
        }
    }


    /**
     * This group is protected because we don't want to expose them for all
     * post types.
     *
     * For example, post_type == attachment use the same WP $post properties
     * but with different names.
     *
     * For example, an attachment's caption is stored in post_excerpt.
     *
     */
    protected function getPostContent( $mix_fallback = '' ) {

        if ( isset( $this->_obj_post->post_content ) ) {
            return $this->_obj_post->post_content;
        }

        return $mix_fallback;
    }


    protected function getPostContentFiltered( $mix_fallback = '' ) {

        if ( $this->_str_content_filtered !== false ) {
            return $this->_str_content_filtered;
        }

        if ( isset( $this->_obj_post->post_content ) ) {
            $this->_str_content_filtered = $this->filterTheContent( $this->getContent( $mix_fallback ) );

            return $this->_str_content_filtered;
        }

        return $mix_fallback;
    }


    protected function getPostExcerpt( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_excerpt ) ) {
            return $this->_obj_post->post_excerpt;
        }

        return $mix_fallback;
    }


    // ------------------------


    public function getActive() {

        return $this->_bool_active;
    }

    public function getID( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->ID ) ) {
            return $this->_obj_post->ID;
        }

        return $mix_fallback;
    }

    protected function getPost() {

        if ( $this->_obj_post instanceof \WP_Post ) {
            return $this->_obj_post;
        }

        return false;
    }

    public function getAuthorID( $mix_fallback = '' ) {

        if ( isset( $this->_obj_post->post_author ) ) {
            return $this->_obj_post->post_author;
        }

        return $mix_fallback;
    }

    public function getDate( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_date ) ) {
            return $this->_obj_post->post_date;
        }

        return $mix_fallback;
    }

    public function getDateGMT( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_date_gmt ) ) {
            return $this->_obj_post->post_date_gmt;
        }

        return $mix_fallback;
    }


    public function getTitle( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_title ) ) {
            return $this->_obj_post->post_title;
        }

        return $mix_fallback;
    }


    public function getStatus( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_status ) ) {
            return $this->_obj_post->post_status;
        }

        return $mix_fallback;
    }

    public function getStatusComment( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->comment_status ) ) {
            return $this->_obj_post->comment_status;
        }

        return $mix_fallback;
    }

    public function getStatusPing( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->ping_status ) ) {
            return $this->_obj_post->ping_status;
        }

        return $mix_fallback;
    }


    public function getPassword( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_password ) ) {
            return $this->_obj_post->post_password;
        }

        return $mix_fallback;
    }

    public function getSlug( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_name ) ) {
            return $this->_obj_post->post_name;
        }

        return $mix_fallback;
    }


    public function getToPing( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->to_ping ) ) {
            return $this->_obj_post->to_ping;
        }

        return $mix_fallback;
    }


    public function getPinged( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->pinged ) ) {
            return $this->_obj_post->pinged;
        }

        return $mix_fallback;
    }


    public function getModified( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_modified ) ) {
            return $this->_obj_post->post_modified;
        }

        return $mix_fallback;
    }

    public function getModifiedGMT( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_modified_gmt ) ) {
            return $this->_obj_post->post_modified_gmt;
        }

        return $mix_fallback;
    }

    public function getParent( $mix_fallback = 0 ) {

        if ( isset( $this->_obj_post->post_parent ) ) {
            return $this->_obj_post->post_parent;
        }

        return $mix_fallback;
    }


    public function getGUID( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->guid ) ) {
            return $this->_obj_post->guid;
        }

        return $mix_fallback;
    }

    public function getOrder( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->menu_order ) ) {
            return $this->_obj_post->menu_order;
        }

        return $mix_fallback;
    }

    public function getType( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_type ) ) {
            return $this->_obj_post->post_type;
        }

        return $mix_fallback;
    }

    public function getMimeType( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_mime_type ) ) {
            return $this->_obj_post->post_mime_type;
        }

        return $mix_fallback;
    }

    public function getCommentCount( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->comment_count ) ) {
            return $this->_obj_post->comment_count;
        }

        return $mix_fallback;
    }


    public function getFilter( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->filter ) ) {
            return $this->_obj_post->filter;
        }

        return $mix_fallback;
    }

    // --------------------------------------------


    /**
     * TODO - don't wait til add_action('setup_theme')?
     *
     * @return mixed
     */
    public function getURL() {

        if ( $this->_str_permalink !== false ) {
            return $this->_str_permalink;
        }
        $int_id               = $this->getID();
        $this->_str_permalink = get_permalink( $int_id );

        return $this->_str_permalink;
    }

    public function getURLShort( $bool_allow_slugs = true ) {

        if ( $this->_str_shortlink !== false ) {
            return $this->_str_shortlink;
        }
        $int_id               = $this->getID();
        $this->_str_shortlink = wp_get_shortlink( $int_id, 'post', $bool_allow_slugs );

        return $this->_str_shortlink;
    }

    /**
     * ref: https://codex.wordpress.org/Function_Reference/get_post_class
     *
     * @param array $mix_add
     * @param bool  $bool_implode
     *
     * @return string
     */
    public function getClass( $mix_add = [], $bool_implode = false ) {

        if ( ! is_array( $this->_arr_post_class ) ) {

            $post_id               = $this->getID();
            $this->_arr_post_class = get_post_class( $mix_add, $post_id );
        }

        if ( $bool_implode === false ) {
            return $this->_arr_post_class;
        } elseif ( is_array( $this->_arr_post_class ) ) {
            return implode( ' ', $this->_arr_post_class );
        }

        return '';
    }


    // ----------------------------------------------


    protected function getAuthor() {

        if ( $this->_obj_author instanceof ClassAuthor ) {
            return $this->_obj_author;
        }

        if ( $this->_int_author_id !== false ) {

            $new               = new ClassAuthor();
            $bool              = $new->setUserByID( $this->_int_author_id );
            $this->_obj_author = $new;

            return $new;
        }

        return false;
    }


    protected function getMeta() {

        if ( $this->_int_id === false ) {
            return false;
        }

        // do we already have the meta?
        if ( $this->_obj_meta instanceof ClassMeta ) {
            return $this->_obj_meta;
        }

        $mix_get_post_meta = get_post_meta( $this->_int_id );

        if ( ! is_array( $mix_get_post_meta ) ) {
            $mix_get_user_meta = [];
        }

        $new  = new ClassMeta();
        $bool = $new->setMeta( $mix_get_post_meta );
        // TODO test $bool
        $this->_obj_meta = $new;

        return $this->_obj_meta;
    }

    /**
     * A post can have many taxonomies. fire up a taxonomies class
     *
     * @return bool|ClassTaxonomies
     */
    protected function getTaxonomies() {

        if ( $this->_obj_taxs instanceof ClassTaxonomies ) {
            return $this->_obj_taxs;
        }

        if ( $this->_obj_post instanceof \WP_Post ) {

            $new = new ClassTaxonomies();
            $new->setPostObject( $this->_obj_post );
            $this->_obj_taxs = $new;

            return $new;
        }

        // TODO return WP_Errow
        return false;
    }

}