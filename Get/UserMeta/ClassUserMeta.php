<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\UserMeta;

use WPezSuite\WPezAPI\Get\Meta\ClassMeta;

class ClassUserMeta extends ClassMeta{

    protected $_int_id;
    protected $_obj_user_meta;


    public function __construct() {

        $this->setPropertyDefaults();
        parent::__construct();
    }

    protected function setPropertyDefaults() {

        $this->_obj_user_meta = false;
    }

    public function setUserByID( $mix = false ) {

        $bool_return = false;
        if ( $mix !== false ) {

            if ( $mix instanceof \WP_User ) {

                $this->_int_id = absint( $mix->ID );
            } else {

                // TODO https://kellenmace.com/check-if-user-exists-by-id-in-wordpress/
                $this->_int_id = absint( $mix );
            }

            $arr_get_user_meta = get_user_meta( $this->_int_id );

            $bool_return = true;
            if ( ! is_array( $arr_get_user_meta ) ) {
                $arr_get_user_meta = [];
                $bool_return       = false;
            }

            $this->setMeta( $arr_get_user_meta );

        }

        return $bool_return;
    }

    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'all':
                return $this->getAll();

            case 'first_name':
            case 'name_first':
                return $this->getNameFirst();

            case 'last_name':
            case 'name_last':
                return $this->getNameLast();

            // name is NOT native to WP
            case 'name':
            case 'first_last_name':
            case 'name_first_last':
                return $this->getNameFirstLast();


            case 'nickname':
            case 'name_nick':
            case 'name_nickname':
                return $this->getNickname();

            case 'desc':
            case 'bio':
            case 'bio_info':
            case 'bio_desc':
            case 'description':
                return $this->getDescription();

            case 'capabilities':
            case 'wp_capabilities':
                return $this->getCapabilities();

            case 'admin_color':
                return $this->getAdminColor();

            case 'closedpostboxes_page':
                return $this->getClosedpostboxesPage();

            case 'primary_blog':
                return $this->getPrimaryBlog();

            case 'rich_editing':
                return $this->getRichEditing();

            case 'source_domain':
                return $this->getSourceDomain();

            default:
                return parent::__get( $str_prop );

        }
    }

    protected function getMaster( $str_prop, $mix_fallback ) {

        $mix = $this->getSingle($str_prop);

        if ( ! empty( $mix ) ) {

            return $mix;
        }

        return $mix_fallback;
    }


    public function getNameFirst( $mix_fallback = '' ) {

        return $this->getMaster( 'first_name', $mix_fallback );

    }


    public function getNameLast( $mix_fallback = '' ) {

        return $this->getMaster( 'last_name', $mix_fallback );

    }

    public function getNameFirstLast( $mix_fallback = '' ){

        $str = $this->getNameFirst() . ' ' . $this->getNameLast();
        if ( ! empty($str )){
            return $str;
        }
        return $mix_fallback;
    }

    public function getNameNickname( $mix_fallback = '' ) {

        return $this->getNickname( $mix_fallback );

    }

    public function getNickname( $mix_fallback = '' ) {

        return $this->getMaster( 'nickname', $mix_fallback );

    }

    public function getDescription( $mix_fallback = '' ) {

        return $this->getMaster( 'description', $mix_fallback );

    }


    public function getCapabilities( $mix_fallback = '' ) {

        return $this->getMaster( 'wp_capabilities', $mix_fallback );

    }

    public function getAdminColor( $mix_fallback = '' ) {

        return $this->getMaster( 'admin_color', $mix_fallback );

    }

    public function getClosedpostboxesPage( $mix_fallback = '' ) {

        return $this->getMaster( 'closedpostboxes_page', $mix_fallback );

    }

    public function getPrimaryBlog( $mix_fallback = '' ) {

        return $this->getMaster( 'primary_blog', $mix_fallback );

    }

    public function getRichEditing( $mix_fallback = '' ) {

        return $this->getMaster( 'rich_editing', $mix_fallback );

    }


    public function getSourceDomain( $mix_fallback = '' ) {

        return $this->getMaster( 'source_domain', $mix_fallback );

    }

}

