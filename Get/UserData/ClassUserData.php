<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\UserData;


class ClassUserData {

    protected $_obj_user_data;
    protected $_mix_default;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_obj_user_data = false;
        $this->_mix_default = false;

    }

    public function setData( $obj = false ) {

        if ( $obj instanceof \stdClass ) {

            $this->_obj_user_data = $obj;

            return true;
        }

        return false;
    }

    public function getDefault( $mix = false ){

        $this->_mix_default = $mix;
        return true;
    }

    public  function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                return $this->getID();

            case 'login':
                return $this->getLogin();

            case 'password':
            case 'pass':
                return $this->getPassword();

            case 'name_nice':
            case 'nicename':
            case 'nice_name':
            case 'namenice':
                return $this->getNameNice();

            case 'email':
                return $this->getEmail();

            case 'website':
            case 'website_url':
            case 'url_website':
            case 'user_url';
                return $this->getWebsiteUrl();

            case 'registered':
                return $this->getRegistered();

            case 'registered_date':
                return 'TODO';

            case 'registered_year':
                return 'TODO';

            case 'registed_month':
                return 'TODO';

            case 'registered_day';
                return 'TODO';

            case 'registered_time';
                return 'TODO';

            case 'activation_key':
                return $this->getActivationKey();

            case 'status':
                return $this->getStatus();

            case 'name_display':
            case 'display_name':
                return $this->getNameDisplay();

            default:
                return $this->_mix_default;

        }
    }

    protected function getMaster( $str_prop, $mix_fallback ) {

        if ( isset( $this->_obj_user_data->$str_prop ) ) {

            return $this->_obj_user_data->$str_prop;

        }

        return $mix_fallback;
    }


    public function getID( $mix_fallback = '' ) {

        return $this->getMaster('ID', $mix_fallback);

    }

    public function getLogin( $mix_fallback = '' ) {

        return $this->getMaster('user_login', $mix_fallback);

    }

    public function getPassword( $mix_fallback = '' ) {

        return $this->getMaster('user_pass', $mix_fallback);

    }

    public function getNameNice( $mix_fallback = '' ) {

        return $this->getMaster('user_nicename', $mix_fallback);

    }

    public function getEmail( $mix_fallback = '' ) {

        return $this->getMaster('user_email', $mix_fallback);

    }

    public function getWebsiteUrl( $mix_fallback = '' ) {

        return $this->getMaster('user_url', $mix_fallback);

    }

    public function getRegistered( $mix_fallback = '' ) {

        return $this->getMaster('user_registered', $mix_fallback);

    }

    public function getActivationKey( $mix_fallback = '' ) {

        return $this->getMaster('user_activation_key', $mix_fallback);

    }

    public function getStatus( $mix_fallback = '' ) {

        return $this->getMaster('user_status', $mix_fallback);

    }

    public function getNameDisplay( $mix_fallback = '' ) {

        return $this->getMaster('display_name', $mix_fallback);

    }
}