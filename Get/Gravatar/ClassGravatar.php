<?php
/**
 * WordPress' get_avatar() function wrapped up as an ez to use class. Establish
 * so it can be reference from a post (object) or a comment (object), or stand
 * alone. https://codex.wordpress.org/Function_Reference/get_avatar
 */

/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Gravatar;

use WPezSuite\WPezAPI\Get\GravatarAvatar\ClassGravatarAvatar;
use WPezSuite\WPezAPI\Get\GravatarProfile\ClassGravatarProfile;


class ClassGravatar {

    protected $_str_user_email;
    protected $_obj_avatar;
    protected $_obj_profile;

    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_str_user_email = false;
        $this->_obj_avatar     = false;
        $this->_obj_profile    = false;


    }

    public function setUserEmail( $str_email = false ) {

        $str_email = trim( $str_email );
        if ( get_avatar_url( $str_email ) !== false ) {
            $this->_str_user_email = $str_email;

            return true;
        }

        return false;
    }

    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {
            case 'avatar':
                return $this->getAvatar();

            case 'profile':
                return $this->getProfile();

            default:
                return false;

        }

    }

    public function getAvatar() {

        if ( $this->_obj_avatar === false && $this->_str_user_email !== false ) {

            $new  = new ClassGravatarAvatar();
            $bool = $new->setUserEmail( $this->_str_user_email );
            if ( $bool === true ) {
                $this->_obj_avatar = $new;
            }

        }

        return $this->_obj_avatar;

    }

    public function getProfile() {

        if ( $this->_obj_profile === false && $this->_str_user_email !== false ) {

            $new  = new ClassGravatarProfile();
            $bool = $new->setUserEmail( $this->_str_user_email );
            if ( $bool === true ) {
                $this->_obj_profile = $new;
            }

        }

        return $this->_obj_profile;

    }

}