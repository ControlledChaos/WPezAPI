<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezAPI\Get\Taxonomy;

use WPezSuite\WPezAPI\Get\Terms\ClassTerms as ClassTerms;


class ClassTaxonomy {

    protected $_mix_ret;
    protected $_int_post_id;
    protected $_obj_tax;
    protected $_obj_terms;
    protected $_bool_has_terms;


    public function __construct( $obj_std = false ) {

        $this->setPropertyDefaults();

        if ( $obj_std instanceof \stdClass ) {
            return $this->setTaxonomyObject( $obj_std );
        }
    }

    protected function setPropertyDefaults() {

        $this->_mix_ret        = false;
        $this->_obj_terms = false;
        $this->_bool_has_terms = null;

    }


    // TODO
    private function setTaxWPError( $obj ) {

        if ( $obj instanceof \WP_Error ) {
            $this->_obj_tax_wp_error = $obj;
        }
    }


    public function setTaxonomyObject( $obj = false ) {

        if ( $obj instanceof \WP_Taxonomy ) {

            $this->_obj_tax = $obj;

            return true;

        }
        return false;
    }

    public function setPostID( $int = false ) {

        if ( $int !== false ) {
            $this->_int_post_id = (integer)$int;

            return true;
        }

        return false;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'name':
                return $this->getName();

            case 'type':
            case 'object_type':
                return $this->getObjectType();

            // https://codex.wordpress.org/Function_Reference/get_ancestors
            // not to be confused with 'labels' (plural) below
            case 'label':
                return $this->getLabel();

            case  'hierarchical':
                return $this->getHierarchical();

            case 'update_count_callback':
                return $this->getUpdateCountCallback();

            case 'rewrite':
                return $this->getRewrite();

            case 'query_var':

                return $this->getQueryVar();

            case 'public':
                return $this->getPublic();

            case 'show_ui':
            case 'ui':
                return $this->getShowUI();

            case 'show_tagcloud':
            case 'tagcloud':
                return $this->getShowTagcloud();

            // TODO?
            case 'builtin':
            case '_builtin':
                return $this->getBuiltin();

            // not to be confused with 'label' (singular) above
            case 'labels':
                return $this->getLabels();

            case 'show_in_nav_menus':
            case 'nav_menus':
                return $this->getShowInNavMenus();

            case 'cap':
                return $this->getCap();

            case 'has_terms':
                return $this->hasTerms();

            case 'terms':

                return $this->getTerms();

            default:
                return $this->_mix_ret;

        }

    }

    public function hasTerms() {

        if ( $this->_bool_has_terms === null ) {
            // ref: https://codex.wordpress.org/Function_Reference/has_term
            $this->_bool_has_terms = has_term( '', $this->getName(), $this->_int_post_id );
        }

        return $this->_bool_has_terms;
    }

    public function getTerms() {

        if ( $this->_obj_terms instanceof ClassTerms ){
            return $this->_obj_terms;
        }

        // A taxonomy (singular) can have many terms,
        $new = new ClassTerms();
        $new->setPostID( $this->_int_post_id );
        $new->setTaxonomy( $this->getName() );

        $this->_obj_terms = $new;

        return $this->_obj_terms;
    }

    public function getName( $mix_fallback = null ) {

        return $this->getMaster( 'name', $mix_fallback );
    }

    public function getObjectType( $mix_fallback = null ) {

        return $this->getMaster( 'object_type', $mix_fallback );
    }

    public function getLabel( $mix_fallback = null ) {

        return $this->getMaster( 'label', $mix_fallback );
    }

    public function getHierarchical( $mix_fallback = null ) {

        return $this->getMaster( 'hierarchical', $mix_fallback );
    }

    public function getUpdateCountCallback( $mix_fallback = null ) {

        return $this->getMaster( 'update_count_callback', $mix_fallback );
    }

    public function getRewrite( $mix_fallback = null ) {

        return $this->getMaster( 'rewrite', $mix_fallback );
    }

    public function getQueryVar( $mix_fallback = null ) {

        return $this->getMaster( 'query_var', $mix_fallback );
    }

    public function getPublic( $mix_fallback = null ) {

        return $this->getMaster( 'public', $mix_fallback );
    }


    public function getShowUI( $mix_fallback = null ) {

        return $this->getMaster( 'show_ui', $mix_fallback );
    }


    public function getShowTagcloud( $mix_fallback = null ) {

        return $this->getMaster( 'show_tagcloud', $mix_fallback );
    }


    public function getBuiltin( $mix_fallback = null ) {

        return $this->getMaster( 'builtin', $mix_fallback );
    }

    public function getLabels( $mix_fallback = null ) {

        return $this->getMaster( 'labels', $mix_fallback );
    }

    public function getShowInNavMenus( $mix_fallback = null ) {

        return $this->getMaster( 'show_in_nav_menus', $mix_fallback );
    }

    public function getCap( $mix_fallback = null ) {

        return $this->getMaster( 'cap', $mix_fallback );
    }


    protected function getMaster( $str_prop = false, $mix_fallback = null ) {

        $str_prop = trim( $str_prop );
        if ( isset( $this->_obj_tax->$str_prop ) ) {
            return $this->_obj_tax->$str_prop;
        }
        if ( $mix_fallback !== null ) {
            return $mix_fallback;
        }

        return $this->_mix_ret;
    }

}